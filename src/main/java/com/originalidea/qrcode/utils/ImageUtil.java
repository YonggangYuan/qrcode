package com.originalidea.qrcode.utils;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageUtil {

    /**
     * 对图片进行放大
     * @param originalImage 原始图片
     * @param times 放大倍数
     * @return
     */
    public static BufferedImage zoomInImage(BufferedImage  originalImage, Integer times){
        int width = originalImage.getWidth()*times;
        int height = originalImage.getHeight()*times;
        BufferedImage newImage = new BufferedImage(width,height,originalImage.getType());
        Graphics g = newImage.getGraphics();
        g.drawImage(originalImage, 0,0,width,height,null);
        g.dispose();
        return newImage;
    }

    /**
     * 对图片进行放大
     * @param srcPath 原始图片路径(绝对路径)
     * @param newPath 放大后图片路径（绝对路径）
     * @param times 放大倍数
     * @return 是否放大成功
     */
    public static boolean zoomInImage(String srcPath,String newPath,Integer times){
        BufferedImage bufferedImage = null;
        try {
            File of = new File(srcPath);
            if(of.canRead()){
                bufferedImage =  ImageIO.read(of);
            }
        } catch (IOException e) {
            //TODO: 打印日志
            return false;
        }
        if(bufferedImage != null){
            bufferedImage = zoomInImage(bufferedImage,times);
            try {
                //TODO: 这个保存路径需要配置下子好一点
                ImageIO.write(bufferedImage, "JPG", new File(newPath)); //保存修改后的图像,全部保存为JPG格式
            } catch (IOException e) {
                // TODO 打印错误信息
                return false;
            }
        }
        return true;
    }
    /**
     * 对图片进行缩小
     * @param originalImage 原始图片
     * @param times 缩小倍数
     * @return 缩小后的Image
     */
    public static BufferedImage zoomOutImage(BufferedImage originalImage, Integer times){
        int width = originalImage.getWidth()/times;
        int height = originalImage.getHeight()/times;
        BufferedImage newImage = new BufferedImage(width,height,originalImage.getType());
        Graphics g = newImage.getGraphics();
        g.drawImage(originalImage, 0,0,width,height,null);
        g.dispose();
        return newImage;
    }
    /**
     * 对图片进行缩小
     * @param srcPath 源图片路径（绝对路径）
     * @param newPath 新图片路径（绝对路径）
     * @param times 缩小倍数
     * @return 保存是否成功
     */
    public static boolean zoomOutImage(String srcPath,String newPath,Integer times){
        BufferedImage bufferedImage = null;
        try {
            File of = new File(srcPath);
            if(of.canRead()){
                bufferedImage =  ImageIO.read(of);
            }
        } catch (IOException e) {
            //TODO: 打印日志
            return false;
        }
        if(bufferedImage != null){
            bufferedImage = zoomOutImage(bufferedImage,times);
            try {
                //TODO: 这个保存路径需要配置下子好一点
                ImageIO.write(bufferedImage, "JPG", new File(newPath)); //保存修改后的图像,全部保存为JPG格式
            } catch (IOException e) {
                // TODO 打印错误信息
                return false;
            }
        }
        return true;
    }
    
//  private void imageZoom() {
//      // 图片允许最大空间 单位：KB
//      double maxSize = 400.00;
//      // 将bitmap放至数组中，意在bitmap的大小（与实际读取的原文件要大）
//      ByteArrayOutputStream baos = new ByteArrayOutputStream();
//      bitMap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//      byte[] b = baos.toByteArray();
//      // 将字节换成KB
//      double mid = b.length / 1024;
//      // 判断bitmap占用空间是否大于允许最大空间 如果大于则压缩 小于则不压缩
//      if (mid > maxSize) {
//          // 获取bitmap大小 是允许最大大小的多少倍
//          double i = mid / maxSize;
//          // 开始压缩 此处用到平方根 将宽带和高度压缩掉对应的平方根倍
//          // （1.保持刻度和高度和原bitmap比率一致，压缩后也达到了最大大小占用空间的大小）
//          bitMap = zoomImage(bitMap, bitMap.getWidth() / Math.sqrt(i),
//                  bitMap.getHeight() / Math.sqrt(i));
//      }
//  }
//
//  /***
//   * 图片的缩放方法
//   * 
//   * @param bgimage
//   *            ：源图片资源
//   * @param newWidth
//   *            ：缩放后宽度
//   * @param newHeight
//   *            ：缩放后高度
//   * @return
//   */
//  public static Bitmap zoomImage(Bitmap bgimage, double newWidth,
//          double newHeight) {
//      // 获取这个图片的宽和高
//      float width = bgimage.getWidth();
//      float height = bgimage.getHeight();
//      // 创建操作图片用的matrix对象
//      Matrix matrix = new Matrix();
//      // 计算宽高缩放率
//      float scaleWidth = ((float) newWidth) / width;
//      float scaleHeight = ((float) newHeight) / height;
//      // 缩放图片动作
//      matrix.postScale(scaleWidth, scaleHeight);
//      Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width, (int) height, matrix, true);
//      return bitmap;
//  }
}