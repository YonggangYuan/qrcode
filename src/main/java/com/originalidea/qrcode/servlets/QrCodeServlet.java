package com.originalidea.qrcode.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.originalidea.qrcode.application.QRCodeFactory;

/**
 * 
 * @author Yonggang Yuan
 *
 */

public class QrCodeServlet extends HttpServlet {

    private static final long serialVersionUID = -5860063597533264503L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String qrcodeSorce = request.getParameter("qrcode-source");
        if(qrcodeSorce == null || "".equals(qrcodeSorce.trim())) {
            // TODO : 此处，没想好该怎么处理，目前这种处理方式不优雅但是很合理
            return;
        }
        if(qrcodeSorce.length() > 140) {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write("{\"success\":false}");
            // TODO:定位到error页面,文本内容太长
            return;
        }
        // TODO : 此处的路劲不太合适,#1：写死不好；#2：多用户情况会存在BUG
        String relativePath = "assets/qrcodes/qrcode.png";
        String realPath = request.getRealPath("/" + relativePath);
        if(QRCodeFactory.getQrcode(qrcodeSorce, realPath)) {
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write("{\"success\":true, \"content\":\"" + relativePath + "\"}");
        }
    }

}
