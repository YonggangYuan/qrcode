/**
 * @author Yonggang Yuan
 */

// TODO : Now, we use jquery only, but it seems that we should use node.js or requirejs

/**
 * Textarea内容有变化之后自动提交表单。
 * #1: 必须让右边的图片区显示Loading...... 
 * #2 : 不再变化后显示图片(没有文字时显示默认图片)
 * #3 : 对所有的给定的字符常量必须改成变量形式
 */

var defalutImagePath = "assets/images/160x160.png";

// 实现类似于java中的trim的方法
String.prototype.trim=function(){
   return this.replace(/(^\s*)|(\s*$)/g, "");
};

String.prototype.headerTrim=function(){
   return this.replace(/(^\s*)/g,"");
};

String.prototype.tailTrim=function(){
   return this.replace(/(\s*$)/g,"");
};


// AJAX自动提交, 
// TODO : BUG : 刷新时机差一步
function autoSubmit() {
    var msg = document.getElementById("qrcode-source").value;
    if(!msg || msg.trim()=="") {
        document.getElementById("qrcode-img"). setAttribute("src", defalutImagePath);
        viewDisabled();
        downloadDisabled();
        return;
    }
    $.ajax({
        type: "POST",
        url: 'code',
        data: 'qrcode-source=' + msg,
        dataType: 'json',
        success: function(backData){
            if(backData.success) {
                document.getElementById("qrcode-img"). setAttribute("src", backData.content);
                viewActive(backData.content);
                downloadActive(backData.content);
            } else {
                document.getElementById("qrcode-img"). setAttribute("src", defalutImagePath);
                viewDisabled();
                downloadDisabled();
            }
        }
     }); 
}

// 处理下载二维码的事件
function download() {
    var downloadUrl = document.getElementById("qrcode-download"). getAttribute("href");
    if(!downloadUrl || downloadUrl.trim()=="" || downloadUrl.trim()=="#" || downloadUrl.trim()==defalutImagePath) {
        return;
    }
    var msg = document.getElementById("qrcode-source").value;
    if(!msg || msg.trim()=="") {
        document.getElementById("qrcode-img"). setAttribute("src", defalutImagePath);
        viewDisabled();
        downloadDisabled();
        return;
    }
    // 执行一次AJAX请求(必须在下载前执行请求)
    autoSubmit();
    // TODO  3 : 设置httpheader或者别的方式，实现下载
    return true;
}

//处理预览二维码的事件
function view() {
    var viewUrl = document.getElementById("qrcode-view"). getAttribute("href");
    if(!viewUrl || viewUrl.trim()=="" || viewUrl.trim()=="#" || viewUrl.trim()==defalutImagePath) {
        document.getElementById("qrcode-img"). setAttribute("src", defalutImagePath);
        viewDisabled();
        downloadDisabled();
        return;
    }
    var msg = document.getElementById("qrcode-source").value;
    if(!msg || msg.trim()=="") {
        document.getElementById("qrcode-img"). setAttribute("src", defalutImagePath);
        viewDisabled();
        downloadDisabled();
        return;
    }
    // 执行一次AJAX请求(必须在下载前执行请求)
    autoSubmit();
    // TODO  3 : 设置httpheader或者别的方式，实现下载
}

// 
function viewActive (url) {
    document.getElementById("qrcode-view").setAttribute("href", url);
    document.getElementById("qrcode-view").setAttribute("target", "_blank");
}

// 
function downloadActive (url) {
    document.getElementById("qrcode-download").setAttribute("href", url);
    document.getElementById("qrcode-download").setAttribute("target", "_blank");
}

//
function viewDisabled () {
    document.getElementById("qrcode-view").setAttribute("href", "#");
    document.getElementById("qrcode-view").setAttribute("target", "_self");
}

// 
function downloadDisabled () {
    document.getElementById("qrcode-download").setAttribute("href", "#");
    document.getElementById("qrcode-download").setAttribute("target", "_self");
}

// 图片直接下载
function saveImage(img) {
}

